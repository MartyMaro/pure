love.filesystem.setRequirePath("util/?.lua;lib/?.lua;lib/?/init.lua;?.lua;?/init.lua")

require "string"
require "table"
require "delta"

lg, ls, lf = love.graphics, love.system, love.filesystem -- love
PI, inf, abs, floor, ceil, min, max, deg, rad, random, randomseed = math.pi, 1e309, math.abs, math.floor, math.ceil, math.min, math.max, math.deg, math.rad, math.random, math.randomseed -- math

vector = require "vector" -- Light vector 2D (hump)
timer = require "timer" -- Delayed code execution (hump)
gamestate = require "gamestate" -- Scenes (hump), modified to support 30log
fadestate = require "fadestate" -- Fading animations for gamestate (hump)
beholder = require "beholder" -- key events
anim8 = require "anim8" -- animations
adm = require "adm" -- admob wrapper
uad = require "uad" -- unityads wrapper
slib = require "slib" -- encrypted save game
cargo = require "cargo" -- lua loader
scrale = require "scrale" -- screen scaler
prof = require "jprof.jprof" -- profiler

Concord = require("concord")
Entity = require("concord.entity")
Component = require("concord.component")
System = require("concord.system")
Instance = require("concord.instance")

hc = require "hc" -- HardonCollider

settings = {}
save = function() slib.saveE(data.save) end

local clear

function love.load(args)
	math.randomseed(os.time())
	love.settings(settings)
	lf.setIdentity(lf.getIdentity(), not DEBUG) -- allow modding on debug
	clear = settings.clearScreen

	if DEBUG then
		require "util.debug"
		inspect = require "inspect"
		debug.initsh()
	end

	scrale.init({
		fillHorizontally = settings.fillAxis == "x",
		fillVertically = settings.fillAxis == "y",
	})
	IS_MOBILE = scrale.isMobile()

	Concord:init({ useEvents = false })

	data = cargo.init("data")
	C = cargo.init("ecs/components")
	F = cargo.init("ecs/factory")
	S = cargo.init("ecs/systems")
	prefab = cargo.init("prefabs")
	scene = cargo.init("scenes")

	slib.init("slib", "save", settings.saveKey);

	data.quad:init()
	
	fadestate.register(gamestate, nil, { "update", "draw" })
	fadestate.switch(scene[settings.firstScene])

	if not TESTADS then
		adm.init(data.ads.bMain, "bottom", data.ads.iMain)
		uad.init(data.ads.iDemoU)
	else
		adm.init(data.ads.bDemo, "bottom", data.ads.iDemo)
		uad.init(data.ads.iDemoU)
	end
end

function love.update(dt) prof.push("frame")
	-- print("█████████████████████████████████████████████████████████")
	timer.update(dt)
	fadestate.update(dt)
end

function love.draw() 
	scrale.drawOnCanvas(clear)
end

function love.focus(f)
	frozen = not f and IS_MOBILE
end

function love.keypressed(k, code, isrepeat)
	if k == "+" then
		scrale.toggleFullscreen()
	elseif k == "escape" then
		love.event.quit(0)
	end
end

function love.errhand(msg)
	local msg = (debug.traceback("Error: " .. tostring(msg), 1+(layer or 1)):gsub("\n[^\n]+$", ""))
	love.window.showMessageBox("Fatal Error", msg) -- TODO: Report error to the dev (lua-sendmail + google smtp)
	love.event.quit(1)
end

function love.quit()
	local r = love.window.showMessageBox("", "Exit?", { "OK", "Cancel" }) ~= 1
	if not r and PROF_CAPTURE then
		love.filesystem.createDirectory("prof")
		prof.write("prof/" .. os.date("%Y_%m_%d_%H_%M_%S") .. ".prof")
	end
	return r
end