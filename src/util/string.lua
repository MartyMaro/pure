﻿local random = math.random

function string.starts(s, sub)
   return string.sub(s, 1, string.len(sub)) == sub
end

function string.ends(s, sub)
   return sub == "" or string.sub(s,-string.len(sub)) == sub
end

function string.sformat(s, ...)
  -- replace flags $1, $2, etc with corresponding arguments
  local n = { ... }
  for i = 1, #n do
    s = string.gsub(s, "$" .. i, n[i])
  end
  return s
end

function string.split(s, sep)
	assert(s)
	assert(sep)
	local t={} ; i=1
	for str in string.gmatch(s, "([^"..sep.."]+)") do
		t[i] = str
		i = i + 1
	end
	return t
end

function string.toColor(s)
	s = s:gsub("%{", ""):gsub("%}", ""):gsub("%S", "")
	return string.split(s, ",")
end

function string.newUuid() -- call math.randomseed(os.time()) somewhere in your code
	local t = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx"
	return string.gsub(t, '[xy]', function (c)
			local v = (c == 'x') and random(0, 0xf) or random(8, 0xb)
			return string.format('%x', v)
	end)
end