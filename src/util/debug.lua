﻿function debug.condentityprint(entity, requiredname, ...)
	if entity._name == requiredname then print("CEP", "[" .. requiredname .. "]", ...) end
end

function debug.printif(cond, ...)
	if cond then print("PIF", ...) end
end

local printedonce = false
function debug.printonce(...)
	if not printedonce then
		print(...)
	end
	printedonce = true
end

function debug.globals()
  return _G
end

function debug.upvalues()
  local vars = {}
  local idx = 1
  local func = debug.getinfo(2, "f").func
  while true do
    local ln, lv = debug.getupvalue(func, idx)
    if ln ~= nil then
      vars[ln] = lv
    else
      break
    end
    idx = 1 + idx
  end
  return vars
end

function debug.locals()
  local vars = {}
  local idx = 1
  while true do
    local ln, lv = debug.getlocal(2, idx)
    if ln ~= nil then
      vars[ln] = lv
    else
      break
    end
    idx = 1 + idx
  end
  return vars
end

debug.blockstr = "███████████████████"

-- SHORTHANDS --
function debug.initsh()
	cep = debug.condentityprint
	pif = debug.printif
	pon = debug.printonce
	glo = debug.globals
	upv = debug.upvalues
	loc = debug.locals
	sck = debug.traceback
	z0r = debug.blockstr
end