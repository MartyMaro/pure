﻿if delta == nil then delta = {} end

function delta.staged(dt, dtlimit, func)
	local dtleft = dt
	while dtleft > 0 do
		func(min(dtlimit, dtleft))
		dtleft = dtleft - dtlimit
	end
end