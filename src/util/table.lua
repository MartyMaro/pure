﻿local abs, floor, ceil, min, max = math.abs, math.floor, math.ceil, math.min, math.max

--- Counts the total number of elements in a table
-- @param t Table
-- @return Total number of elements
function table.count(t)
  assert(t, "table is nil")
  local n = 0
  for _ in pairs(t) do
    n = n + 1
  end
  return n
end

--- Counts the number of non-nil duplicate values in a table
-- @param t Table
-- @return Number of duplicate values
local temp = {}
function table.dcount(t)
  assert(t, "table is nil")
  local d = 0
  for _, v in pairs(t) do
    if temp[v] then
      d = d + 1
    end
    temp[v] = true
  end
  table.clear(temp)
  return d
end

--- Removes all values in a table
-- @param t Table
function table.clear(t)
  assert(t, "table is nil")
  for i in pairs(t) do
    t[i] = nil
  end
end

--- Reverses the elements in a list
-- @param t Table
-- @param r Resulting table (optional)
-- @return Reversed table
function table.reverse(t, r)
  assert(t, "table is nil")
  r = r or t
  local n = #t
  if t == r then
    for i = 1, floor(n/2) do
      local i2 = n - i + 1
      r[i], r[i2] = r[i2], r[i]
    end
  else
    for i, v in ipairs(t) do
      r[n - i + 1] = v
    end
  end
  return r
end

--- Finds the first occurrence of a value in a list
-- @param t Table
-- @param s Search value
-- @param o Starting index (optional)
-- @return Numeric index of the found value
function table.find(t, s, o)
  assert(t, "table is nil")
  o = o or 1
  for i = o, #t do
    if t[i] == s then
      return i
    end
  end
end

--- Finds the last occurrence of a value in a list
-- @param t Table
-- @param s Search value
-- @param o Starting index (optional)
-- @return Numeric index of the found value
function table.rfind(t, s, o)
  assert(t, "table is nil")
  o = o or #t
  -- iterate in reverse
  for i = o, 1, -1 do
    if t[i] == s then
      return i
    end
  end
end

--- Copies the contents from one table to another
--- Does not remove existing elements in the destination table
-- @param s Source table
-- @param d Destination table (optional)
-- @param c Cache table (internal)
-- @return The resulting or destination table
local cache = {}
function table.copy(s, d, c)
  assert(s, "table is nil")
  assert(s ~= d, "source and destination tables must be different")
  d = d or {}
  -- clear the cache, initially
  if c == nil then
    c = cache
    table.clear(c)
  end
  -- copy elements from the source table
  for k, v in pairs(s) do
    if type(v) == "table" then
      if c[v] then
        -- reference cycle
        d[k] = c[v]
      else
        -- recursive copy
        if d[k] == nil then
          d[k] = {}
        end
        c[v] = d[k]
        table.copy(v, d[k], c)
      end
    else
      d[k] = v
    end
  end
  return d
end

--- Replaces the existing values in a table
--- Optionally, converts the source values to match the destination types
-- @param s Source table
-- @param d Destination table
-- @param m Convert source values to destination types (optional)
-- @return The number of replaced values
local function toboolean(v)
  if v == 'true' then
    return true
  elseif v == 'false' then
    return false
  end
end
function table.replace(s, d, m)
  assert(s, "source table is nil")
  assert(d, "destination table is nil")
  assert(s ~= d, "source and destination tables must be different")
  local n = 0
  -- iterate destination table
  for k, dv in pairs(d) do
    local sv = s[k]
    local dt = type(dv)
    local st = type(sv)
    if st == "table" and dt == "table" then
      -- recursive
      n = n + table.replace(sv, dv, m)
    else
      -- convert source value to destination type
      if m == true and dt ~= st then
        if dt == 'boolean' then
          sv = toboolean(sv)
        elseif dt == 'number' then
          sv = tonumber(sv)
        elseif dt == 'string' then
          sv = tostring(sv)
        else
          sv = nil
        end
      end
      -- replace destination value
      if sv ~= nil and dv ~= sv then
        d[k] = sv
        n = n + 1
      end
    end
  end
  return n
end

--- Asserts that a given string is a valid concatenated key
-- @ck Concatenated key
function table.vckey(ck)
  -- must be a string
  local t = type(ck)
  if t ~= "string" then
    local e = string.format("key is not a string '%s'", tostring(ck))
    assert(false, e)
  end
  -- must have length greater than 0
  local len = string.len(ck)
  if len == 0 then
    assert(false, "key is empty")
  end
  -- cannot end with the percent sign ('%')
  -- cannot end with the dot symbol ('.') except for ('%.')
  local lc = string.byte(ck, len)
  local nlc = string.byte(ck, len - 1)
  if (lc == 46 and nlc ~= 37) or (lc == 37) then
    local e = string.format("invalid termination character in key '%s'", ck)
    assert(false, e)
  end
end

--- Returns a table node and its key from a concatenated key
--- The concatenated key is delimited by the dot ('.') character
-- @param t Table
-- @param ck Concatenated key
-- @param m Create nested tables if non-existent (optional)
-- @return Sub-table and key from table t
local keys = {}
function table.ck(t, ck, m)
  -- parse keys
  -- handle special sequence ('%.')
  ck = string.gsub(ck, "%%%.", "\001")
  for k in string.gmatch(ck, "([^%.]+)") do
    k = string.gsub(k, "\001", ".")
    table.insert(keys, k)
  end
  -- descend down the table
  local c = t
  local d
  for i, k in ipairs(keys) do
    -- allow access for numeric keys
    if c[k] == nil then
      local nk = tonumber(k)
      if c[nk] then
        k = nk
      end
    end
    -- create tables if key is non-existing
    if i < #keys then
      local kt = type(c[k])
      if kt ~= "table" then
        if m == true then
          c[k] = {}
        else
          c, d = nil, nil
          break
        end
      end
      c = c[k]
    end
    d = k
  end
  -- clear the buffer tables
  table.clear(keys)
  return c, d
end

--- Accesses a table value using a concatenated key
-- @param t Table
-- @param ck Concatenated key
-- @return Value from table t
function table.get(t, ck)
  assert(t, "table is nil")
  table.vckey(ck)
  local s, k = table.ck(t, ck, false)
  if s == nil or k == nil then
    return
  end
  return s[k]
end

--- Assigns the given value in a table using a concatenated key
--- Creates sub-tables if necessary
-- @param t Table
-- @param ck Concatenated key
-- @param v Value
function table.set(t, ck, v)
  assert(t, "table is nil")
  table.vckey(ck)
  local s, k = table.ck(t, ck, true)
  s[k] = v
end

--- Returns a list of all concatenated keys in a table
--- Breadth first traversal provides the shortest concatenated keys
-- @param t Table
-- @param keys Output table (optional)
-- @return List of concatenated keys
local queue = {}
-- temporary lookup for visited tables
local visited = {}
local ckeys = {}
function table.gckeys(t, keys)
  assert(t, "table is nil")
  keys = keys or {}
  table.insert(queue, t)
  while #queue > 0 do
    -- dequeue
    local c = table.remove(queue, 1)
    -- queue child nodes
    visited[c] = true
    for k, v in pairs(c) do
      -- supported keys types: string or number
      local kt = type(k)
      if kt == 'string' or kt == 'number' then
        local ck = tostring(k)
        -- handle special sequence ('%.')
        ck = string.gsub(ck, "%.", "%%%.")
        local p = ckeys[c]
        if p then
          ck = string.format("%s.%s", p, ck)
        end
        table.insert(keys, ck)
        if type(v) == "table" then
          if not visited[v] then
            ckeys[v] = ck
            table.insert(queue, v)
          end
        end
      end
    end
  end
  -- optional, alphabetic sort
  -- WARN: "1.10" is sorted above "1.2"
  table.sort(keys)
  -- clear the buffer tables
  table.clear(visited)
  table.clear(ckeys)
  return keys
end

--- Returns an iterator providing pairs of concatenated key and value
-- @param t Table
-- @return Iterator
function table.ckeys(t)
  assert(t, "table is nil")
  local keys = table.gckeys(t)
  local i = 0
  return function()
    i = i + 1
    if i > #keys then
      return
    end
    return keys[i], table.get(t, keys[i])
  end
end

--- Safely remove items from an array table while iterating (disallows break but is faster)
table.ripairs = function(t)
	-- Try not to use break when using this function;
	-- it may cause the array to be left with empty slots
	local ci = 0
	local remove = function()
		t[ci] = nil
	end
	return function(t, i)
		i = i+1
		ci = i
		local v = t[i]
		if v == nil then
			local rj = 0
			for ri = 1, i-1 do
				if t[ri] ~= nil then
					rj = rj+1
					t[rj] = t[ri]
				end
			end
			for ri = rj+1, i do
					t[ri] = nil
			end
			return
			end
		return i, v, remove
	end, t, ci
end

--- Safely remove items from an array table while iterating (allows break but is slower)
table.rtipairs = function(t, skip_marked)
  local ci = 0
  local tbr = {} -- "to be removed"
  local remove = function(i)
    tbr[i or ci] = true
  end
  return function(t, i)
    local v
    repeat
      i = i+1
      v = t[i]
    until not v or not (skip_marked and tbr[i])
    ci = i
    if v == nil then
      local rj = 0
      for ri = 1, i-1 do
        if not tbr[ri] then
          rj = rj+1
          t[rj] = t[ri]
        end
      end
      for ri = rj+1, i do
        t[ri] = nil
      end
      return
    end
    return i, v, remove
  end, t, ci
end

--- Performs an insertion sort
function table.isort(t)
local len = #t
local j
for j = 2, len do
		local key = t[j]
		local i = j - 1
		while i > 0 and t[i] > key do
				t[i + 1] = t[i]
				i = i - 1
		end
		t[i + 1] = key
end
return t
end

--- Executes the function e for all elements in the table, passes the current value of the table to the passed function.
function table.exec(t, e)
	for _, v in pairs(t) do
		e(v)
	end
end