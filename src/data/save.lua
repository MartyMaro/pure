return {
	o = 0, -- timestamp
	n = nil, -- locale
	l = 0, -- left handed
	m = 1, -- music volume
	s = 1, -- sound volume
	t = 0, -- tilemap level
	c = { 1, 1 }, -- coordinates
	g = 0, -- gold
	d = 0, -- diamonds (prem)
	r = nil, -- rated
	i = 0, -- total score of all levels
	j = {}, -- high score array for each levelmap
	-- ctrl = { -- controls
	-- 	["f10"] = "DEBUG",
	-- 	["space"] = "JUMP",
	-- 	["point"] = "JUMP",
		-- ["w"] = "MOVE-Y-UP",
		-- ["s"] = "MOVE-Y-DOWN",
		-- ["a"] = "MOVE-X-LEFT",
		-- ["d"] = "MOVE-X-RIGHT",
	-- },
	-- ctrlv = 1
}