local os, a = love.system.getOS(), {}

if os == "Android" then

	a.bDemo = "ca-app-pub-3940256099942544/6300978111" -- banner [AdMob]
	a.iDemo = "ca-app-pub-3940256099942544/1033173712" -- interstitial [AdMob]
	-- a.iDemo = "ca-app-pub-3940256099942544/8691691433" -- interstitial (video) [AdMob]
	a.rDemo = "ca-app-pub-3940256099942544/5224354917" -- reward [AdMob]

	-- add your own ad IDs for Android here:
	a.bMain = "ca-app-pub-3904538280812276/8827361727"
	a.iMain = "ca-app-pub-3904538280812276/4034625077"
	-- a.rFactorizeWin = "ca-app-pub-3904538280812276/7035924714"
	-- a.rGetCoins = "ca-app-pub-3904538280812276/8270304706"

elseif os == "iOS" then
	
	a.bDemo = "ca-app-pub-3940256099942544/2934735716" -- banner [AdMob]
	a.iDemo = "ca-app-pub-3940256099942544/4411468910" -- interstitial [AdMob]
	-- a.iDemo = "ca-app-pub-3940256099942544/5135589807" -- interstitial (video) [AdMob]
	a.rDemo = "ca-app-pub-3940256099942544/1712485313" -- reward [AdMob]

	-- add your own ad IDs for iOS here:
	a.bMain = "ca-app-pub-3904538280812276/6619353986"
	a.iMain = "ca-app-pub-3904538280812276/1339107141"
	-- a.rFactorizeWin = "ca-app-pub-3904538280812276/1666410209"
	-- a.rGetCoins = "ca-app-pub-3904538280812276/3898022845"

end

a.iDemoU = "video" -- interstitial [UnityAds]
a.rDemoU = "rewardVideo" -- reward [UnityAds]

a.iMainU = "video"
a.rFactorizeWinTypeU = "doubleWin"
a.rGetCoinsU = "getCoins"

a.rDemoType = "coins" -- reward type

-- add your own reward types here:
a.rFactorizeWinType = "FactorizeCoinWin" a.rFactorizeWinValueU = 2
a.rGetCoinsType = "Coins" a.rGetCoinsValueU = 200

return a