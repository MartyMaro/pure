return {
	["monkus.a"] = (function(c, x, y, w, h)
		local cx, cy = x + w / 2, y + h / 2
		return c:polygon(cx,y , x+w-w/7,y+h/7 , x+w,cy , cx-w/4,y+h , cx-w/4,y+h/1.5)
	end),
	["vinesegm"] = (function(c, x, y, w, h)
		return c:rectangle(x, y, w, h) 
	end),
	["treeblock"] = (function(c, x, y, w, h)
		return c:rectangle(x + w / 2 - 42, y + 10, 84, h - 20) 
	end),
}