return {
	["monkus.a"] = {
		size = { 64, 64 },
		states = {
			{ type = "idle", dir = "any", frames = 2, delay = 0.5 },
			{ type = "jump", dir = "up", frames = 1, delay = inf },
			{ type = "jump", dir = "down", frames = 1, delay = inf },
			{ type = "grab", dir = "any", frames = 4, delay = 0.09, joint = { 60,10 } },
		},
		defaultState = 1,
	}
}