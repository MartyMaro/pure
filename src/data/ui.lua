local button = { "default", "active" }
local toggleButton = { "default", "active", "pressed" }

return {
	["playButton.ui"] = button,
	["playBigButton.ui"] = button,
	["doubleWinButton.ui"] = button,
	["shopButton.ui"] = button,
	["leaderboardsButton.ui"] = button,
	["archivementsButton.ui"] = button,
	["cogPauseToggleButton.ui"] = toggleButton,
	["musicToggleButton.ui"] = toggleButton,
	["sfxToggleButton.ui"] = toggleButton,
	["rateButton.ui"] = button,
	["globeButton.ui"] = toggleButton,
	["infoButton.ui"] = button,
	["getCoinsButton.ui"] = button,
	["musicToggleButton.ui"] = button,
	["shopItemToggleButton.ui"] = toggleButton,
	["buyButton.ui"] = button,
}