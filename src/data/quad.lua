local quad = {}

function quad:init()
	local ani, ui = data.ani, data.ui
	for name, dat in pairs(data.sprite) do
		if not ani[name] then
			if ui[name] then
				self[name] = lg.newQuad(dat.x, dat.y, dat.width, dat.height / #ui[name], dat["total_width"], dat["total_height"])
			else
				self[name] = lg.newQuad(dat.x, dat.y, dat.width, dat.height, dat["total_width"], dat["total_height"])
			end
		end
	end
end

return quad