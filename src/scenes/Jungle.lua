scene = {}

function scene:init()
	self.instance = Instance()

	self.renderSystem = S.Render()
	self.instance:addSystem(self.renderSystem, "update", nil, false)
	self.instance:addSystem(self.renderSystem, "draw", nil, false)

	if DEBUG then	self.debugSystem = S.Debug(settings.debugKey, data.font.default) self.instance:addSystem(self.debugSystem, "draw", nil, false)	end
end

function scene:enter(previous)
	self.instance:enableSystem(self.renderSystem, "update")
	self.instance:enableSystem(self.renderSystem, "draw")

	self.player = Entity()
	F.Object(self.player, "monkus.a", 40, 40, 1)
	self.player:apply()
	self.instance:addEntity(self.player)

	self.cliff = Entity()
		:give(C.Sprite, "cliff")
		:give(C.Position, 50, 50, 0)
		:apply()
	self.instance:addEntity(self.cliff)

	if DEBUG then	self.instance:enableSystem(self.debugSystem, "draw") end
end

function scene:update(dt) if frozen then return end prof.push("update")
	self.instance:emit("update", dt)
prof.pop("update") end

function scene:draw() prof.push("draw")
	self.instance:emit("draw")
prof.pop("draw") prof.pop("frame") end

function scene:leave()
	self.instance:clear()
	self.cliff = nil

	self.instance:disableSystem(self.renderSystem, "update")
	self.instance:disableSystem(self.renderSystem, "draw")

	if DEBUG then self.instance:disableSystem(self.debugSystem, "draw")	end
end

return scene