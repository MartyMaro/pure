local FS = {
  _VERSION     = "fadestate v0.1.0",
  _DESCRIPTION = "Fade transitions for hump.gamestate",
  _URL         = "",
  _LICENSE     = [[
    MIT LICENSE
    Copyright (c) 2018 Martin Braun
    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:
    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ]]
}

local function __NULL__() end
local OPAC_TOP = 255

local abs, floor, ceil, min, max = math.abs, math.floor, math.ceil, math.min, math.max
local lg, stack, intermState = love.graphics, {}, {}
local lastState, fadeOutState, fadeOutStateAfterFn, currentFadeOpts, hadState, pushes

local defFadeOpts = {
	style = "out-in", -- out-in, cross
	delay = 0.5, -- delay in seconds until transition has completed
	callback = function(newState, oldState) return false end, -- callback after fade transition
}

-- canvases of states, opacities of states, gamestate api
local C, O, GS = setmetatable({}, { __mode = "k" }), setmetatable({}, { __mode = "k" }), nil

local function leaveLastState()
	if fadeOutState then
		O[fadeOutState] = 0
		if not pushes then
			(fadeOutState.leaveAfterFadeOut or __NULL__)(fadeOutState)
		end
		fadeOutState = nil
		fadeOutStateAfterFn = nil
	end
end

local function applyMissingDefaultFadeOptions(opts)
	assert(opts and type(opts) == "table", "Wrong argument: Fade options is not a table.")
	for k, v in pairs(defFadeOpts) do
		opts[k] = opts[k] or v
	end
end

local function initStateChange(to, fadeOptions)
	assert(not fadeOutState, "There is still an active transition. Prevent any further transitions until it's finished.")
	currentFadeOpts = fadeOptions or {}
	applyMissingDefaultFadeOptions(currentFadeOpts)
	if hadState then
		fadeOutState = GS.current()
		lastState = fadeOutState
	end
	if currentFadeOpts.style == "out-in" then
		O[to] = 0
	elseif currentFadeOpts.style == "cross" then
		O[to] = 255
	end
end

function FS.fading()
	return fadeOutState
end

function FS.getStateCanvas(state)
	return C[state]
end

function FS.register(gamestateApi, defaultFadeOptions, callbacks)
	GS = gamestateApi
	defFadeOpts = defaultFadeOptions or defFadeOpts
	assert(GS and type(GS) == "table" and GS.switch and GS.push and GS.pop and GS.current and GS.registerEvents, "Wrong argument: GS is not a gamestate object.")
	assert(type(defFadeOpts) == "table", "Wrong argument: Default fade options is not a table.")

	GS.registerEvents(callbacks)
	local ld = love.draw
	love.draw = function(...) -- override love.draw
		ld(...)
		local c, f, r = GS.current(), fadeOutState, nil
		if C[c] then
			lg.setColor({ 255, 255, 255, O[c] })
			lg.draw(C[c])
			if f then
				lg.setColor({ 255, 255, 255, O[f] })
				r = lg.draw(C[f]) or r
			end
			r = lg.setColor({ 255, 255, 255, 255 }) or r
			if love.drawend and type(love.drawend) == "function" then
				r = love.drawend(...) or r
			end
		end
		return r
	end
	setmetatable(GS, {__index = function(_, func) -- forward any undefined functions
		return function(...)
			local c, f, r = GS.current(), fadeOutState, nil
			if c then
				r = (c[func] or __NULL__)(c, ...) or r
			end
			if f then
				r = (f[func] or __NULL__)(f, ...) or r -- call update, draw, etc. on fading-out-state, too
			end
			return r
		end
	end})
	fadestate.initState(intermState, 1, 1)
end

function FS.initState(state, cw, h)
	if not C[state] then
		state.leaveAfterFadeOut = state.leave -- move leave function away to prevent to early call by GS
		state.leave = __NULL__
		if not cw and h then wc, h, _ = love.window.getMode() end
		C[state] = h and love.graphics.newCanvas(cw, h) or cw
	end
end

function FS.switch(to, fadeOptions, ...)
	pushes = false
	fadeOptions = fadeOptions or {}
	if stack[#stack] == to or fadeOptions.style == "out-turn-in" and stack[#stack] then
		fadeOptions.delay = (fadeOptions.delay or 1) / 4
		fadeOptions.style = "out-in"
		local callback = fadeOptions.callback or defFadeOpts.callback
		local callbackVarargs = { nil, fadeOptions }
		for i = 1, select("#", ...) do
			callbackVarargs[#callbackVarargs + 1] = select(i, ...)
		end
		fadeOptions.callback = function()
			fadeOptions.callback = callback
			callbackVarargs[1] = to
			FS.switch(unpack(callbackVarargs))
		end
		FS.switch(intermState, fadeOptions)
	else
		if fadeOptions.style == "out-turn-in" then
			fadeOptions.style = "out-in"
		end
		stack[#stack] = to
		initStateChange(to, fadeOptions)
		local varargs = { ... }
		table.insert(varargs, 1, to)
		GS.switch(unpack(varargs))
		hadState = true
	end
end

function FS.push(to, fadeOptions, ...)
	assert(stack[#stack] ~= to, "Pushing to the same gamestate is not possible. Use switch instead.")
	pushes = true
	stack[#stack+1] = to
	initStateChange(to, fadeOptions)
	GS.push(to, ...)
	hadState = true
end

function FS.pop(fadeOptions, ...)
	pushes = false
	local to = stack[#stack-1]
	stack[#stack] = nil
	initStateChange(to, fadeOptions)
	GS.pop(...)
end

function FS.update(dt)
	if currentFadeOpts then
		local curState = GS.current()
		local opacChange = OPAC_TOP / currentFadeOpts.delay * dt
		if fadeOutState then
			O[fadeOutState] = O[fadeOutState] - opacChange
			if O[fadeOutState] < 0 then
				leaveLastState()
			end
		end
		if not fadeOutState then
			if curState and O[curState] < OPAC_TOP then
				O[curState] = min(O[curState] + opacChange, OPAC_TOP)
			elseif currentFadeOpts.callback then
				if type(currentFadeOpts.callback) == "function" then
					currentFadeOpts.callback(GS.current(), FS.fading())
				end
				currentFadeOpts.callback = nil
			end
		end
	end
end

function FS.drawOnStateCanvas(state, clear)
	love.graphics.setCanvas(C[state])
	if clear then
		love.graphics.clear()
	end
end

return FS