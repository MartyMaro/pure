local component = Component(function(self, r)
	self.r = r
end)

function component:translate(r)
	self.r = self.r + r
end

return component