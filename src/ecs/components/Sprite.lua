local ani, ui, sprite, quad = data.ani, data.ui, data.sprite, data.quad
local component = Component(function(self, name)
	self.name = name
	local s, a = sprite[name], ani[name]
	if a then
		self.w, self.h = a.size[1], a.size[2]
		local g = anim8.newGrid(self.w, self.h, s["total_width"], s["total_height"], s.x - SB_SEGM_BORDER, s.y - SB_SEGM_BORDER, SB_SEGM_BORDER)
		self.animations = {}
		for i, v in ipairs(a.states) do
			local newAni = anim8.newAnimation(g("1-" .. v.frames, i), v.delay)
			self.animations[v.type..":"..v.dir] = newAni
			if a.defaultState == i then self.ani = newAni end
		end
	else
		self.ui = ui[name]
		self.quad = quad[name]
		self.w, self.h = s.width, s.height
	end
end)

function component:translate(state, direction)
	if self.animations then
		self.ani = self.animations[state..":"..dir] or self.animations[state..":any"] or self.ani
	elseif self.ui then
	end
end

return component