local component = Component(function(self, x, y, z)
	self.x = x
	self.y = y
	self.z = z or 0
end)

function component:translate(x, y, z)
	self.x = self.x + x
	self.y = self.y + y
	self.z = self.z + (z or 0)
end

return component