local component = Component(function(self)
end)

function component:on(eventName, func)
	self[eventName] = func
end

return component