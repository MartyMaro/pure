local component = Component(function(self, x, y)
	self.x = x
	self.y = y
end)

function component:translate(x, y, z)
	self.x = self.x + x
	self.y = self.y + y
end

return component