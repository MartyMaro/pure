local component = Component(function(self, w, h)
	self.w = w
	self.h = h
end)

function component:resize(w, h)
	self.w = w or self.w
	self.w = h or self.h
end

return component