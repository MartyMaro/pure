local component = Component(function(self, x, y, z)
	self.x = x
	self.y = y
	self.z = z
end)

function component:translate(x, y)
	self.x = self.x + x
	self.y = self.y + y
end

return component