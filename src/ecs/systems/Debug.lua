local system, name = System( { C.Position, "positionPool" } ), "DebugSystem"
local maxmem = 0

function system:init(renderButton, font)
	self.renderButton = renderButton
	self.font = font
end

function system:entityAdded(e)
end

function system:draw() prof.push(name..".draw")
	lg.setFont(self.font)
	if love.keyboard.isDown(self.renderButton) then
		for _, e in ipairs(self.positionPool.objects) do
			local s, p, d, o, r, x, y, w, h, ox, oy, rr = e:get(C.Sprite), e:get(C.Position), e:get(C.Size), e:get(C.Offset), e:get(C.Rotation)
			x, y = floor(p.x), floor(p.y)
			if d then 
				w, h = d.w, d.h 
			elseif s then 
				w, h = s.w, s.h 
			end
			if o then
				ox, oy = o.x, o.y
			else
				ox, oy = 0, 0
			end
			rr = r and r.r or 0
			lg.push()
			lg.translate(x, y)
			lg.setColor(255, 0, 0)
			lg.circle("fill", 0, 0, 2)
			lg.rotate(rr)
			lg.translate(-ox, -oy)
			lg.setColor(255, 255, 0)
			lg.rectangle("line", 0, 0, w, h)
			lg.setColor(255, 255, 0)
			if w >= 64 and h >= 64 then
				lg.printf(string.format("%.0f", x) .. "," .. string.format("%.0f", y) .. "," .. string.format("%.0f", w) .. "," .. string.format("%.0f", h), 0, 0, w, "left", 0, 1, 1, -2, -2)
				if s then
					lg.printf(s.name, 0, h - 12, w, "left", 0, 1, 1, -2, -2)
				end
			end
			lg.pop()
		end
	end

	local fps = love.timer.getFPS()
	local fpsstr = (fps < 100 and "0" or "") .. (fps < 10 and "0" or "") .. tostring(fps)
	local mem = collectgarbage("count") / 1000
	maxmem = max(mem, maxmem)
	local memstr = (mem < 1000 and "0" or "") .. (mem < 100 and "0" or "") .. (mem < 10 and "0" or "") .. tostring(floor(mem))
	local maxmemstr = (maxmem < 1000 and "0" or "") .. (maxmem < 100 and "0" or "") .. (maxmem < 10 and "0" or "") .. tostring(floor(maxmem))
	local sba = S.Render.getSpriteBatch():getCount()
	local sbastr = (sba < 1000 and "0" or "") .. (sba < 100 and "0" or "") .. (sba < 10 and "0" or "") .. tostring(sba)
	local sbamaxstr = (SB_SIZE < 1000 and "0" or "") .. (SB_SIZE < 100 and "0" or "") .. (SB_SIZE < 10 and "0" or "") .. tostring(SB_SIZE)
	local dcs = lg.getStats().drawcalls
	local dcsstr = (dcs < 100 and "0" or "") .. (dcs < 10 and "0" or "") .. tostring(dcs)

	lg.setColor(0, 255, 0)
	lg.print("FPS: " .. fpsstr .. "   •   MEM (MB): " .. memstr .. " (" .. maxmemstr .. ")  •  SB: " .. sbastr .. " / " .. sbamaxstr .. "  •  DC: " .. dcsstr, 5, 10)
	lg.setColor(255, 255, 255)
prof.push(name..".draw") end

function system:entityRemoved(e)
end

return system