local system, name = System( { C.Sprite, C.Position } ), "RenderSystem"
local sb function system.getSpriteBatch() return sb end
local sortFn = function(a, b) return a:get(C.Position).z < b:get(C.Position).z end -- TODO: Improve speed!

function system:init()
	sb = lg.newSpriteBatch(data.sprites, SB_SIZE, "dynamic")
end

-- function system:entityAdded(e)
-- end

function system:update(dt) prof.push(name..".update")
	for _, e in ipairs(self.pool.objects) do
		local s = e:get(C.Sprite)
		if s.ani then
			s.ani:update(dt)
		end
	end
prof.pop(name..".update") end

function system:draw(dt) prof.push(name..".draw")
	sb:clear()
	local objs = self.pool.objects
	table.sort(objs, sortFn) -- TODO: Improve speed!
	for _, e in ipairs(objs) do
		local spr = e:get(C.Sprite)
		local p = e:get(C.Position) local x, y = floor(p.x), floor(p.y)
		local r, rr = e:get(C.Rotation), 0 if r then rr = r.r end
		local s, sx, sy = e:get(C.Scale), 1, 1 if s then sx = s.x sy = s.y end
		local o, ox, oy = e:get(C.Offset), 0, 0 if o then ox = o.x oy = o.y end
		local d = e:get(C.Size)
		if spr.ani then
			sb:add(spr.ani:getFrameInfo(x, y, rr, sx, sy, ox, oy))
		elseif spr.ui then
		elseif d then

		else
			sb:add(spr.quad, x, y, rr, sx, sy, ox, oy)
		end
	end
	lg.draw(sb)
	scrale.draw()
prof.pop(name..".draw") end

-- function system:entityRemoved(e)
-- end

return system