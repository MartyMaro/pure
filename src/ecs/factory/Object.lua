return function(e, name, x, y, z)
	e:give(C.Sprite, name):apply()
	local s = e:get(C.Sprite)
	e:give(C.Position, x, y, z)
	e:give(C.Offset, s.w / 2, s.h / 2)
end