return function(e, name, x, y, z, onHoldBegin, onHold, onHoldEnd, onToggled)
	F.Button(e, name, x, y, z, onHoldBegin, onHold, onHoldEnd)

	local handler = e:get(C.EventHandler)

	handler:on("toggled", function(sender, args)
		if onHoldBegin then onToggled(sender, args) end
	end)

	e:apply()
end