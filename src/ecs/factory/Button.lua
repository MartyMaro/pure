return function(e, name, x, y, z, onHoldBegin, onHold, onHoldEnd)
	e:give(C.Sprite, name):give(C.Position, x, y, z):give(C.EventHandler)

	local handler = e:get(C.EventHandler)

	handler:on("holdbegin", 1, function(sender, args)
		if onHoldBegin then onHoldBegin(sender, args) end
	end)
	
	handler:on("hold", 1, function(sender, args)
		if onHold then onHold(sender, args) end
	end)
	
	handler:on("holdend", 1, function(sender, args)
		if onHoldEnd then onHoldEnd(sender, args) end
	end)

	e:apply()
end